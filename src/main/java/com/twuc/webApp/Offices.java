package com.twuc.webApp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "")
public class Offices {
    @Id
    private Long id;
    @Column(length = 36, nullable = false)
    private String city;

    public Offices(Long id, String city) {
        this.id = id;
        this.city = city;
    }

    public Offices() {
    }

    public Long getId() {
        return id;
    }

    public String getCity() {
        return city;
    }
}
