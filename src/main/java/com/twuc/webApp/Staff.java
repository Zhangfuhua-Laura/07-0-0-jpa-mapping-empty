package com.twuc.webApp;

import com.sun.org.glassfish.gmbal.InheritedAttributes;

import javax.persistence.*;

@Entity
public class Staff {
    @Id
    private Long id;

    @Embedded
    private Name name;

    public Staff(Long id, String firstName, String lastName) {
        this.id = id;
        this.name = new Name(firstName, lastName);
    }

    public Staff() {
    }
}
