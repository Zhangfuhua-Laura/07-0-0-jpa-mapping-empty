package com.twuc.webApp;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OfficesRepository extends JpaRepository<Offices, Long> {
}
