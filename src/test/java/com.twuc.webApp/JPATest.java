package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest(showSql = false)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
class JPATest {
    @Autowired
    private OfficesRepository repo;

    @Autowired
    private EntityManager em;

    @Test
    void hello() {
        assertTrue(true);
    }

    @Test
    void should_insert() {
        Offices offices = repo.save(new Offices(1L, "Xi'an"));
        em.flush();
        assertNotNull(offices);
    }

    @Test
    void should_persist_with_em() {
        em.persist(new Offices(1L, "Xi'an"));
        em.flush();
        em.clear();
        Offices offices = em.find(Offices.class, 1L);
        assertEquals(Long.valueOf(1L), offices.getId());
        assertEquals("Xi'an", offices.getCity());
    }

    @Test
    void should_throw_when_city_is_null() {
        assertThrows(PersistenceException.class,
                () -> {
                    em.persist(new Offices(1L, null));
                    em.flush();
                },
                "City is null");
    }

    @Test
    void should_throw_when_city_is_longer_than_36() {
        assertThrows(DataIntegrityViolationException.class,
                () -> {
                    repo.save(new Offices(1L, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
                    repo.flush();
                });
    }
}
