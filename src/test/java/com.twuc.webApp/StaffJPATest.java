package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest(showSql = false)
public class StaffJPATest {

    @Autowired
    private StaffRepository repo;
    
    @Test
    void should_save_staff() {
        Staff staff = repo.save(new Staff(1L, "Fuhua", "Zhang"));
        assertNotNull(staff);
    }

    @Test
    void should_get_staff() {
    }
}
